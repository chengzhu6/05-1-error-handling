package com.twuc.webApp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class UserController {


    @GetMapping("/api/errors/illegal-argument")
    public void getException() {
        throw new RuntimeException("SN-666");
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<MyError> handleException(RuntimeException e){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new MyError("SN-666",e.getMessage()));
    }


    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<MyError> handleAccessControlException(AccessControlException e){
        return ResponseEntity.status(403)
                .body(new MyError("SN-666",e.getMessage()));
    }

    @GetMapping("/api/errors/null-pointer")
    public void getNullPointException() {
        throw new NullPointerException("null-pointer");
    }


    @GetMapping("/api/exceptions/access-control-exception")
    public void getAccessControlException() {
        throw new AccessControlException("access-control-exception");
    }




}
