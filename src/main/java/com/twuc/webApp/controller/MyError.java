package com.twuc.webApp.controller;

public class MyError {

    private String errorCode;

    private String errorMessage;

    public MyError(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public MyError() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
