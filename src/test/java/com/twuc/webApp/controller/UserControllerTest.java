package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class UserControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;


    @Test
    void should_handle_the_exception() {

        ResponseEntity<MyError> entity = testRestTemplate.getForEntity("/api/errors/illegal-argument", MyError.class);

        assertEquals(entity.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        assertEquals(entity.getBody().getErrorCode(), "SN-666");
        assertEquals(entity.getBody().getErrorMessage(), "SN-666");


    }

    @Test
    void should_not_handle_the_access_control_exception_by_handle_exception_action() {

        ResponseEntity<MyError> entity = testRestTemplate.getForEntity("/api/exceptions/access-control-exception", MyError.class);
        assertEquals(entity.getBody().getErrorCode(), "SN-666");
        assertEquals(entity.getBody().getErrorMessage(), "access-control-exception");
    }

    @Test
    void should_handle_the_exception_throwed_by_handle_exception_action() {

        ResponseEntity<MyError> entity = testRestTemplate.getForEntity("/api/errors/illegal-argument", MyError.class);
        assertEquals(entity.getBody().getErrorCode(), "SN-666");
        assertEquals(entity.getBody().getErrorMessage(), "SN-666");
    }

    @Test
    void should_return_403_when_have_the_more_small_exception_handler() {
        ResponseEntity<MyError> entity = testRestTemplate.getForEntity("/api/exceptions/access-control-exception", MyError.class);
        assertEquals(entity.getStatusCode(), HttpStatus.FORBIDDEN);
        assertEquals(entity.getBody().getErrorCode(), "SN-666");
        assertEquals(entity.getBody().getErrorMessage(), "access-control-exception");
    }


}